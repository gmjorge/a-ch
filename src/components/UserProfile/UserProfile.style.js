import styled from 'styled-components';

const lightGrey = '#A7A7A7';
const blue = '#6DD9D8';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 350px;
  height: 400px;
  border-color: pink;
  box-shadow: 6px 6px 15px -9px rgba(0,0,0,0.47);
`;

export const Image = styled.div`
  position: absolute;
  width: 100px;
  height: 100px;
  background: url(${({url}) => url});
  position: absolute;
  top: calc((100% / 2) - 50px);
  left: calc((100% / 2) - 50px);
  border-radius: 50%;
  background-repeat: no-repeat;
  background-size: contain;
`;

export const ImageContainer = styled.div`
  position: relative;
  height: 200px;
  width: 100%;
  overflow: hidden;
`;

export const ImageBackground = styled.div`
  width: 100%;
  height: 100%;
  background: url(${({url}) => url});
  background-size: cover;
  background-repeat: no-repeat;
  filter: blur(3px);
  background-position: center;
`;

export const ContentContainer = styled.span`
  text-align: center;
  font-size: 14px;
  text-transform: capitalize;
  padding: 16px;
  > h3 {
    color: ${blue};
  }
  p {
    color: ${lightGrey};
  }
`;
