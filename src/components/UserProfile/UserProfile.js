import React from 'react';
import { Container, Image, ImageContainer, ImageBackground, ContentContainer } from './UserProfile.style';

function UserProfile({ user }) {
  return (
    <Container>
      <ImageContainer>
        <ImageBackground url={user.picture}/>
        <Image url={user.photo} />
      </ImageContainer>
      <ContentContainer>
        <h3>{user.fullName}</h3>
        <p>{user.address}</p>
        <p>{user.postalCode}</p>
      </ContentContainer>
    </Container>
  );
}

export default UserProfile;
