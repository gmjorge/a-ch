import React from 'react';
import {shallow} from 'enzyme';
import UserProfile from './UserProfile';


it('renders without crashing', () => {
  const mockedUser = { thumbnail: '', fullName: '' };
  shallow(<UserProfile user={mockedUser} />);
});

it('renders all the content correctly', () => {
  const mockedUser = { thumbnail: '', fullName: 'Jorge Garcia', address: 'Unknown street', postalCode: '123' };
  const component = shallow(<UserProfile user={mockedUser}/>);
  const renderedContent = component.html();

  expect(renderedContent).toContain('Jorge Garcia');
  expect(renderedContent).toContain('Unknown street');
  expect(renderedContent).toContain('123');
})
