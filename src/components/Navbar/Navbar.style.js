import styled from 'styled-components';
import logo from '../../assets/logo.png';

export const NavStyled = styled.a`
  display: inline-block;
  background: white;
  height: 85px;
  width: 100%;
  box-shadow: 0px 6px 15px -9px rgba(0,0,0,0.47);
  background: url(${logo}) no-repeat;
  cursor: pointer;
`;
