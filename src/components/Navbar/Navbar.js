import React from 'react';
import { NavStyled } from './Navbar.style';

function Navbar() {
  return (<NavStyled target="_blank" href="https://athenaworks.io/"/>);
}

export default Navbar;
