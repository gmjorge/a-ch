import React from 'react';
import {InputStyled} from './Searchbar.style';

const Searchbar = ({ onSearch = () => {} }) => {
  const handleOnChange = (event) => {
    const { value } = event.target;
    onSearch(value);
  };

  return (
      <InputStyled placeholder="Type for start searching..." onChange={handleOnChange}/>
  );
}

export default Searchbar;
