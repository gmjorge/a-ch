import styled from 'styled-components';

export const InputStyled = styled.input`
  width: 100%;
  height: 40px;
  font-size: 20px
  padding: 0 16px;
  color: #FF6552;
`;
