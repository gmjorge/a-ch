import React from 'react';
import {shallow} from 'enzyme';
import UserListItem from './UserListItem';


it('renders without crashing', () => {
  const mockedUser = { thumbnail: '', fullName: '' };
  shallow(<UserListItem user={mockedUser} />);
});

it('call the assigned click handler', () => {
  const clickSpy = jest.fn();
  const mockedUser = { thumbnail: '', fullName: '' };
  const component = shallow(<UserListItem user={mockedUser} onClick={clickSpy}/>);
  component.simulate('click');

  expect(clickSpy).toBeCalled();
})
