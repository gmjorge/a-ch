import styled, {css} from 'styled-components';

const hoverColor = '#E4E3E8';
const activeColor = '#6DD9D8';

const activeStyles = css`
  background-color: ${activeColor};
  border-color: ${activeColor};
  color: white;
  box-shadow: 0px 6px 15px -6px rgba(0,0,0,0.47)
`;

const getActiveStyles = ({ active }) => (
  active ? activeStyles : null
);

export const Container = styled.div`
  display: inline-block;
  width: 100%;
`;

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  border-radius: 3px;
  cursor: pointer;
  margin: 4px;
  border: 1px solid transparent;
  height: 40px;
  transition: all 0.2s ease-in-out;
  color: black;

  :hover {
    background-color: ${hoverColor};
    border: 1px solid ${hoverColor};
    color: black;
  }

  ${getActiveStyles}
`;

export const Image = styled.div`
  width: 30px;
  height: 30px;
  border-radius: 50%;
  background: url(${({url}) => url});
  background-repeat: no-repeat;
  background-position: center;
  margin-right: 16px;
  margin-left: 5px;
`;

export const Name = styled.span`
  font-size: 14px;
  text-transform: capitalize;
`;
