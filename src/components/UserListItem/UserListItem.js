import React from 'react';
import { Container, Wrapper, Image, Name } from './UserListItem.style';

function UserListItem({ user, active, onClick = () => {} }) {
  const handleOnClick = () => {
    onClick(user);
  }

  return (
    <Container onClick={handleOnClick}>
      <Wrapper active={active}>
        <Image url={user.thumbnail} />
        <Name>{user.fullName}</Name>
      </Wrapper>
    </Container>
  );
}

export default UserListItem;
