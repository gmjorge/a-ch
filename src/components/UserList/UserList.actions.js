import client from '../../lib/client';
import User from '../../core/user';

export const types = {
  GET_USERS_REQUESTED: 'GET_UESRS_REQUESTED',
  GET_USERS_COMPLETED: 'GET_UESRS_COMPLETED',
  GET_USERS_FAILED: 'GET_UESRS_FAILED',
  SEARCH_USER: 'SEARCH_USER',
};

const toUser = item => new User(item);

export const getUsers = (howMany = 30) => dispatch => {
  dispatch({ type: types.GET_USERS_REQUESTED, payload: null });

  client.get(`/?results=${howMany}`).then((response => {
    dispatch({
      type: types.GET_USERS_COMPLETED,
      payload: response.data.results.map(toUser),
    });
  })).catch(err => {
    dispatch({
      type: types.GET_USERS_FAILED,
      payload: err,
    });
  });
};

export const findUser = (term)  => dispatch => {
  dispatch({ type: types.SEARCH_USER, payload: { term }});
};
