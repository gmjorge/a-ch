import React from 'react';
import {shallow} from 'enzyme';
import UserList from './UserList';


it('renders without crashing', () => {
  const mockedUser = { thumbnail: '', fullName: '' };
  const users = [mockedUser, mockedUser];
  shallow(<UserList users={users} />);
});

it('call the assigned click handler when user click on an item', () => {
  const clickSpy = jest.fn();
  const mockedUser = { thumbnail: '', fullName: '' };
  const users = [mockedUser, mockedUser];
  const component = shallow(<UserList users={users} onClick={clickSpy}/>);
  component.find('UserListItem').first().simulate('click');

  expect(clickSpy).toBeCalled();
})
