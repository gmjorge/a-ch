import React from 'react';
import UserListItem from '../UserListItem/UserListItem';
import { Container, ItemContainer } from './UserList.styles';

function UserList({ users = [], activeUser, onClick= () => {}, limit = 10 }) {
  return (
    <Container>
      {users.map((user, index) =>  {
         if (index < limit) {
           return (
             <ItemContainer key={index}>
               <UserListItem user={user} onClick={onClick} active={activeUser && activeUser.fullName === user.fullName}/>
             </ItemContainer>
           )
         }
         return null;
      })}
    </Container>
  );
}

export default UserList;
