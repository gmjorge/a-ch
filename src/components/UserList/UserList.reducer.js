import {types} from './UserList.actions';

const initialState = {
  items: [],
  filteredUsers: [],
  error: null,
  term: '',
  isLoading: false,
};

const byName = (term) => (user) => (
  user.fullName.includes(term)
);

export default (state = initialState, action) => {
  switch(action.type) {
    case types.GET_USERS_REQUESTED: {
      return {...state, isLoading: true };
    }
    case types.GET_USERS_COMPLETED: {
      return {...state, isLoading: false, items: action.payload };
    }
    case types.GET_USERS_FAILED: {
      return {...state, isLoading: false, error: action.payload };
    }
    case types.SEARCH_USER: {
      const { term } = action.payload;
      const filteredUsers = term ? state.items.filter(byName(term)) : [];
      return { ...state, filteredUsers, term };
    }
    default: {
      return state;
    }
  }
}
