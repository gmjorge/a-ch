import styled from 'styled-components';

export const Container = styled.div`
  width: 900px;
  margin: 24px auto 0 auto;
  display: flex;
`;

export const UsersContainer = styled.div`
  flex-grow: 1;
`;

