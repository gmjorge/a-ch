import React from 'react';
import UserList from '../../components/UserList/UserList';
import UserProfile from '../../components/UserProfile/UserProfile';
import Navbar from '../../components/Navbar/Navbar.js';
import Searchbar from '../../components/Searchbar/Searchbar';
import { Container, UsersContainer } from './App.style';


const renderProfile = (activeUser) => (
  activeUser ? <UserProfile user={activeUser} /> : null
)

const NotResults = ({ searchTerm, usersLength }) => (
  searchTerm && usersLength === 0 ? <h4>Not results found for: "{searchTerm}"</h4> : null
)

class App extends React.Component {
  render() {
    const { users = [], activeUser = null, onClickItem, onSearch, searchTerm } = this.props;

    return (
      <div>
        <Navbar>
          Header....
        </Navbar>
        <Container>
          <Searchbar onSearch={onSearch} />
        </Container>
        <Container>
          <UsersContainer>
            <UserList users={users} onClick={onClickItem} activeUser={activeUser}/>
            <NotResults searchTerm={searchTerm} usersLength={users.length} />
          </UsersContainer>
          {renderProfile(activeUser)}
        </Container>
      </div>
    );
  }
}

export default App;
