import React from 'react';
import { connect } from 'react-redux';
import AppContainer from './containers/App/App';
import {getUsers, findUser} from './components/UserList/UserList.actions';

class App extends React.Component {
  state = {
    activeUser: null,
  }

  componentDidMount() {
    const { getUsers } = this.props;
    getUsers();
  }

  onClickHanlder = (clickedUser) => {
    const activeUser = this.props.users.find(user => user.fullName === clickedUser.fullName);
    if (activeUser) {
      this.setState({ activeUser })
    }
  }

  onSearchHandler = (searchTerm) => {
    this.setState({ activeUser: null })
    this.props.searchUser(searchTerm);
  }

  render() {
    const users = this.props.searchTerm ? this.props.filteredUsers : this.props.users;
    return (
      <AppContainer
        users={users}
        searchTerm={this.props.searchTerm}
        activeUser={this.state.activeUser}
        onClickItem={this.onClickHanlder}
        onSearch={this.onSearchHandler}
      />
    );
  }
}

const mapStateToProps = state => ({
  users: state.users.items,
  filteredUsers: state.users.filteredUsers,
  searchTerm: state.users.term,
});

const mapDispatchToProps = dispatch => ({
  getUsers: () => dispatch(getUsers()),
  searchUser: (term) => dispatch(findUser(term)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
