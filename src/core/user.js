export default class User {
  constructor(data) {
    this._data = data;
  }

  get id() {
    return this._data.id.value;
  }

  get picture() {
    return this._data.picture.medium;
  }

  get photo() {
    return this._data.picture.large;
  }

  get thumbnail() {
    return this._data.picture.thumbnail;
  }

  get fullName() {
    return `${this._data.name.first} ${this._data.name.last}`;
  }

  get email() {
    return this._data.email;
  }

  get phoneNumber() {
    return this._data.phone || this._data.cell;
  }

  get address() {
    const { street, state, city } = this._data.location;
    return `${street}, ${state}, ${city}`;
  }

  get postalCode() {
    const { postcode } = this._data.location;
    return `Postal Code: ${postcode}`;
  }
}
