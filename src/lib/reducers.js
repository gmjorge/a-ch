import { combineReducers } from 'redux';
import users from '../components/UserList/UserList.reducer';
export default combineReducers({
  users,
});
