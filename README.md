This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Athena Works Challange

### Overview

The objective of this test is evaluating if the applicant has the basic skills required to join
AthenaWorks’ development team. Please read carefully the instructions and follow these to give
a solution for the raised problem. Resolution time matters, please try to find a solution as soon
as possible.

### Problem

Build an application to list people, showing images and reacting to user actions.
1 - Build the frontend application using HTML5, CSS, Javascript and any libraries you may
need.
2 - Populate the people list using this API https://randomuser.me.
3 - Implement a local search bar (client side). 

### Solution


Webapp using the following stack:

- React 16
- Storybook for develop every component 
  - Run `yarn storybook` to see the storybook with the components
- Jest and enzyme for testing
- Redux for handling the application state
- Axios to make the calls to randomuser.me API
- Styled components for keep styles rules isolated
- GCP for hosting the application
